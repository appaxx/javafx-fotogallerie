package app;

import app.ui.Diashow;
import java.util.List;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import app.ui.ImageFullSizeView;
import app.ui.ImageFullSizeViewListener;
import app.ui.ImageGridView;
import app.ui.ImageGridViewListener;
import app.util.ImageLoader;
import java.io.File;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;

public class App extends Application implements ImageGridViewListener, ImageFullSizeViewListener {

    public static final String TITLE = "JavaFX Fotogalerie";
    public static String imageDirectory;
    public static final int WIDTH_IN_PX = 1000;
    public static final int HEIGHT_IN_PX = 800;
    public static final int VERTICAL_SPACE_BETWEEN_NODES_IN_PX = 5;
    public static final int HORIZONTAL_SPACE_BETWEEN_NODES_IN_PX = 5;
    public static final int BUTTON_HEIGHT = 40;
    public static final int BUTTON_WIDTH = 40;

    private StackPane rootPanel;
    private ImageFullSizeView imageFullSizeView;
    private List<Image> images;
    private Stage stage;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.setStage(stage);
        setRootPanel(new StackPane());
        imageFullSizeView = new ImageFullSizeView();
        imageFullSizeView.addImageGridViewListener(this);
        Group root = new Group();
        Scene scene = new Scene(root, WIDTH_IN_PX, HEIGHT_IN_PX);
        GridPane grid = initGridPane();
        VBox wrapper = new VBox();
        Button diashowButton = initDiashowButton();
        wrapper.getChildren().add(diashowButton);
        wrapper.getChildren().add(grid);
        getRootPanel().getChildren().add(wrapper);
        ((Group) scene.getRoot()).getChildren().add(getRootPanel());
        imageDirectory = choosePictureFolder(stage).getAbsolutePath();
        setImages(ImageLoader.getImagesFromDirectory(imageDirectory));
        ImageGridView imageGridView = new ImageGridView(images);
        imageGridView.addImageGridViewListener(this);
        grid.add(imageGridView, 0, 0);
        stage.setTitle(TITLE);
        stage.setScene(scene);
        stage.show();
    }

    private File choosePictureFolder(Stage stage) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choose Picture Folder");
        File selectedDirectory = chooser.showDialog(stage);
        return selectedDirectory;
    }

    private GridPane initGridPane() {
        GridPane result = new GridPane();
        result.setHgap(HORIZONTAL_SPACE_BETWEEN_NODES_IN_PX);
        result.setVgap(VERTICAL_SPACE_BETWEEN_NODES_IN_PX);
        result.setPadding(new Insets(HORIZONTAL_SPACE_BETWEEN_NODES_IN_PX,
                HORIZONTAL_SPACE_BETWEEN_NODES_IN_PX,
                VERTICAL_SPACE_BETWEEN_NODES_IN_PX,
                VERTICAL_SPACE_BETWEEN_NODES_IN_PX));
        return result;
    }

    private void presentImageFullSizeViewWithImage(Image image) {
        imageFullSizeView.setImage(image);
        getRootPanel().getChildren().add(imageFullSizeView);
    }

    private void removeImageFullSizeView() {
        getRootPanel().getChildren().remove(imageFullSizeView);
    }

    @Override
    public void imageClickedInImageGridView(Image image) {
        presentImageFullSizeViewWithImage(image);
    }

    @Override
    public void closeButtonClickedInImageFullSizeView() {
        removeImageFullSizeView();
    }

    private Button initDiashowButton() {
        Button b = new Button("Gallerie");
        b.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Diashow d = new Diashow(images);
                d.showPicture(0);
            }
        });

        return b;
    }

    /**
     * @return the images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     * @param images the images to set
     */
    public void setImages(List<Image> images) {
        this.images = images;
    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * @return the rootPanel
     */
    public StackPane getRootPanel() {
        return rootPanel;
    }

    /**
     * @param rootPanel the rootPanel to set
     */
    public void setRootPanel(StackPane rootPanel) {
        this.rootPanel = rootPanel;
    }

}
