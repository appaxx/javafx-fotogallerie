package app.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

public class ImageLoader {

    private static final String FILE_PREFIX = "file://";

    public static List<Image> getImagesFromDirectory(String pathToDirectory) {
        List<Image> result = new ArrayList<Image>();
        File directory = new File(pathToDirectory);
        if (directory.isDirectory()) {
            for (final File f : directory.listFiles()) {
                Image img = new Image(FILE_PREFIX + f.getAbsolutePath());
                result.add(img);
            }
        }
        return result;
    }
}
