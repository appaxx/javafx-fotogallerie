/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.ui;

import java.util.List;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author enigma
 */
public class Diashow {

    List<Image> images;
    ImageView imageView;
    boolean isStopped = false;
    SequentialTransition trans;

    public Diashow(List<Image> images) {
        this.images = images;
        imageView = new ImageView();
        imageView.setFitHeight(Screen.getPrimary().getBounds().getHeight());
        imageView.setPreserveRatio(true);
        StackPane root = new StackPane(imageView);
        Stage stage = createStageFullScreen(root);
        stage.show();
    }

    private Stage createStageFullScreen(StackPane root) {
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setFullScreen(true);
        stage.fullScreenProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                stage.close();
            }
        });
        stage.getScene().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                isStopped = !isStopped;
                trans.pause();
                if (isStopped == false) {
                    trans.play();
                }
            }
        });
        return stage;
    }

    public void showPicture(int index) {
        if (index < images.size()) {
            FadeTransition ft = createFadeInTransition();
            RotateTransition rt = createRotateTransition();
            ParallelTransition pt = new ParallelTransition(ft, rt);
            ScaleTransition sc = createScaleTransition();
            trans = new SequentialTransition(pt, new PauseTransition(Duration.millis(3000)), sc);
            trans.setOnFinished(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    showPicture(index + 1);
                }
            });
            imageView.setImage(images.get(index));
            imageView.setOpacity(0);
            trans.play();
        } else {
            showPicture(0);
        }
    }

    private ScaleTransition createScaleTransition() {
        ScaleTransition sc = new ScaleTransition(Duration.millis(1000), imageView);
        sc.setFromX(1);
        sc.setToX(0);
        sc.setFromY(1);
        sc.setToY(0);
        return sc;
    }

    private RotateTransition createRotateTransition() {
        RotateTransition rt = new RotateTransition(Duration.millis(2000), imageView);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        return rt;
    }

    private FadeTransition createFadeInTransition() {
        FadeTransition ft = new FadeTransition(Duration.millis(1000), imageView);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        return ft;
    }
}
