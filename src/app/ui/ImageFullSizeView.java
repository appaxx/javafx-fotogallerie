package app.ui;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import app.App;

public class ImageFullSizeView extends GridPane {

    private List<ImageFullSizeViewListener> listener;
    private ImageView imageView;
    private final DoubleProperty zoom;
    private final static double ZOOM_DEFAULT = 1;
    private final static double ZOOM_STEP = 0.2;

    public ImageFullSizeView() {
        listener = new ArrayList<ImageFullSizeViewListener>();
        zoom = new SimpleDoubleProperty();
        setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        setRowAndColumnConstraints();
        initAndAddButtons();
        initAndaddImageView();
    }

    public void setImage(Image image) {
        imageView.setImage(image);
    }

    private void setRowAndColumnConstraints() {
        ColumnConstraints firstColumnConstraint = new ColumnConstraints();
        firstColumnConstraint.setMinWidth(App.WIDTH_IN_PX);
        firstColumnConstraint.setMaxWidth(App.WIDTH_IN_PX);
        getColumnConstraints().add(firstColumnConstraint);
        RowConstraints firstRowConstraint = new RowConstraints();
        firstRowConstraint.setMinHeight(App.BUTTON_HEIGHT);
        getRowConstraints().add(firstRowConstraint);
    }

    private void initAndaddImageView() {
        imageView = new ImageView();
        zoom.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable arg0) {
                imageView.setFitWidth(zoom.get() * App.HEIGHT_IN_PX);
                imageView.setFitHeight(zoom.get() * (App.WIDTH_IN_PX - App.BUTTON_HEIGHT));
            }
        });
        zoom.set(ZOOM_DEFAULT);
        imageView.setPreserveRatio(true);
        this.add(imageView, 0, 1);
        setHalignment(imageView, HPos.CENTER);
        setValignment(imageView, VPos.CENTER);
    }

    private void initAndAddButtons() {
        this.add(initZoomButtons(), 0, 0);
        Button closeButton = initCloseButton();
        this.add(closeButton, 0, 0);
        setHalignment(closeButton, HPos.RIGHT);
    }

    private HBox initZoomButtons() {
        HBox result = new HBox();
        result.setSpacing(0);
        Button loadButton = new Button();
        loadButton.setText("+");
        loadButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                zoom.set(zoom.get() + ZOOM_STEP);
            }
        });
        loadButton.setPrefSize(App.BUTTON_WIDTH, App.BUTTON_HEIGHT);

        Button slideButton = new Button();
        slideButton.setText("-");
        slideButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (zoom.get() > (ZOOM_STEP)) {
                    zoom.set(zoom.get() - ZOOM_STEP);
                }
            }
        });
        slideButton.setPrefSize(App.BUTTON_WIDTH, App.BUTTON_HEIGHT);
        result.getChildren().addAll(loadButton, slideButton);
        return result;
    }

    private Button initCloseButton() {
        Button result = new Button();
        result.setText("x");
        result.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                zoom.set(ZOOM_DEFAULT);
                notifyListenersAboutCloseButtonClicked();
            }
        });
        result.setPrefSize(App.BUTTON_WIDTH, App.BUTTON_HEIGHT);
        return result;
    }

    public void addImageGridViewListener(ImageFullSizeViewListener imageFullSizeViewListener) {
        if (imageFullSizeViewListener != null) {
            listener.add(imageFullSizeViewListener);
        }
    }

    public void removeImageGridViewListener(ImageFullSizeViewListener imageFullSizeViewListener) {
        if (imageFullSizeViewListener != null) {
            listener.remove(imageFullSizeViewListener);
        }
    }

    private void notifyListenersAboutCloseButtonClicked() {
        listener.forEach(listener -> listener.closeButtonClickedInImageFullSizeView());
    }
}
