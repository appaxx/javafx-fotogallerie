package app.ui;

import javafx.scene.image.Image;

public interface ImageGridViewListener {

	public void imageClickedInImageGridView(Image image);
}
