package app.ui;

import java.util.ArrayList;
import java.util.List;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import app.App;

public class ImageGridView extends FlowPane {

    private List<ImageGridViewListener> listener;
    private int singleImageWidth;

    public ImageGridView(List<Image> images) {
        listener = new ArrayList<ImageGridViewListener>();
        singleImageWidth = calculateWidthForSingleImageViewFromImagesNumber(images.size());
        this.setVgap(App.VERTICAL_SPACE_BETWEEN_NODES_IN_PX);
        this.setHgap(App.HORIZONTAL_SPACE_BETWEEN_NODES_IN_PX);
        this.setPrefSize(App.WIDTH_IN_PX, App.HEIGHT_IN_PX);
        images.forEach(image -> createImageViewFromImageAndAddToGrid(image, this));
    }

    private void createImageViewFromImageAndAddToGrid(Image image, FlowPane flowPanel) {
        ImageView imageView = new ImageView(image);
        imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                notifyListenersAboutImageViewClicked((ImageView) event.getTarget());
            }
        });
        imageView.setFitWidth(singleImageWidth);
        imageView.setPreserveRatio(true);
        flowPanel.getChildren().add(imageView);
    }

    private static int calculateWidthForSingleImageViewFromImagesNumber(int size) {
        int result = 0;
        double radicalOfSize = Math.sqrt(size);
        double modOfSizeWithRadical = size % radicalOfSize;
        int rows = (int) (modOfSizeWithRadical == 0 ? radicalOfSize : radicalOfSize + 1);
        result = (App.WIDTH_IN_PX - App.HORIZONTAL_SPACE_BETWEEN_NODES_IN_PX * rows - 1) / rows;
        return result;
    }

    public void addImageGridViewListener(ImageGridViewListener imageGridViewListener) {
        if (imageGridViewListener != null) {
            listener.add(imageGridViewListener);
        }
    }

    public void removeImageGridViewListener(ImageGridViewListener imageGridViewListener) {
        if (imageGridViewListener != null) {
            listener.remove(imageGridViewListener);
        }
    }

    private void notifyListenersAboutImageViewClicked(ImageView imageView) {
        listener.forEach(listener -> listener.imageClickedInImageGridView(imageView.getImage()));
    }
}
